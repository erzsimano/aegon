using System;
using System.Collections.Generic;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(Aegon.Ui.Login.LoginEntry), typeof(Aegon.Droid.Ui.View.LoginEntry.LoginEntryRenderer))]

namespace Aegon.Droid.Ui.View.LoginEntry
{
    public class LoginEntryRenderer : EntryRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);
            if (Control != null)
            {
                Control.SetBackground(Resources.GetDrawable(Resource.Drawable.rounded_entry));
                Control.Gravity = Android.Views.GravityFlags.CenterVertical;
                Control.SetPadding(20, 0, 30, 0);
            }
        }
    }
}