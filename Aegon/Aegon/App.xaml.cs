﻿using Aegon.Ui.Login;
using Aegon.Ui.Main;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace Aegon
{
    public partial class App : Application
    {
        public MainPage AppMainPage { get { return mainPage; } }

        private MainPage mainPage;

        public App()
        {
            InitializeComponent();

            MainPage = new LoginPage(this);
        }

        public void navigateToMainPage()
        {
            if (mainPage == null)
            {
                mainPage = new MainPage();
            }
            MainPage = mainPage;
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
