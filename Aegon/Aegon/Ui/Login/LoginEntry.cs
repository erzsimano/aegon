﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Aegon.Ui.Login
{
    public class LoginEntry : Entry
    {
        public static readonly BindableProperty StrokeColorProperty = BindableProperty
            .Create<LoginEntry, Color>(p => p.StrokeColor, Color.Black);

        public Color StrokeColor
        {
            get { return (Color)base.GetValue(StrokeColorProperty); }
            set { base.SetValue(StrokeColorProperty, value); }
        }

        public LoginEntry()
        {
            //HeightRequest = 50;
            HorizontalOptions = LayoutOptions.FillAndExpand;
            VerticalOptions = LayoutOptions.Center;
        }
    }
}
