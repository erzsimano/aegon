﻿using System;
using Xamarin.Forms;

namespace Aegon.Ui.Login
{
    public partial class LoginPage : ContentPage
    {
        private App app;

        public LoginPage(App app)
        {
            InitializeComponent();
            okButton.Clicked += OkButtonClicked;
            this.app = app;
        }

        private void OkButtonClicked(object sender, EventArgs e)
        {
            app.navigateToMainPage();
        }
    }
}
