﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace Aegon.Ui.Main
{
    public partial class MainPage : MasterDetailPage
    {
        public MainPage()
        {
            InitializeComponent();

            navDrawerPage.ListView.ItemSelected += OnItemSelected;
        }

        private void OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var item = e.SelectedItem as NavDrawerItems.NavDrawerItem;
            if (item != null)
            {
                NavigationPage navPage = new NavigationPage(item.TargetPage);
                this.Title = item.Title;
                navPage.Title = item.Title;
                Detail = navPage;
                navDrawerPage.ListView.SelectedItem = null;
                this.IsPresented = false;
            }
        }
    }
}
