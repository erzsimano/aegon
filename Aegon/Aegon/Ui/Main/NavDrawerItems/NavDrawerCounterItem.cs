﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aegon.Ui.Main.NavDrawerItems
{
    public class NavDrawerCounterItem : NavDrawerItem
    {
        public int Counter { get; set; }
    }
}
