﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Aegon.Ui.Main.NavDrawerItems
{
    public class NavDrawerItem
    {
        public string Title { get; set; }
        public string IconSource { get; set; }
        public Page TargetPage { get; set; }
    }
}
