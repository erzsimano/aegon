﻿using Aegon.Ui.Main.ContentPages.NewsInfo;
using Aegon.Ui.Main.NavDrawerItems;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace Aegon.Ui.Main
{
    public partial class NavDrawerPage : ContentPage
    {
        public ListView ListView { get { return listView; } }

        public NavDrawerCounterItem NewsInfoItem;
        public NavDrawerCounterItem MessagesItem;

        NewsInfoPage newsInfoPage = new NewsInfoPage();

        public NavDrawerPage()
        {
            InitializeComponent();

            var masterPageItems = new List<NavDrawerItem>();

            NewsInfoItem = new NavDrawerCounterItem
            {
                Title = "Hírek, információk",
                IconSource = "news.png",
                Counter = 0,
                TargetPage = newsInfoPage
            };
            masterPageItems.Add(NewsInfoItem);

            MessagesItem = new NavDrawerCounterItem
            {
                Title = "Üzenetek",
                IconSource = "envelope_gray.png",
                Counter = 0,
                TargetPage = newsInfoPage
            };
            masterPageItems.Add(MessagesItem);
            masterPageItems.Add(new NavDrawerItem
            {
                Title = "Egyéni számla",
                IconSource = "personal_account.png",
                TargetPage = newsInfoPage
            });
            masterPageItems.Add(new NavDrawerItem
            {
                Title = "Szolgáltatási számla",
                IconSource = "provision_account.png",
                TargetPage = newsInfoPage
            });
            masterPageItems.Add(new NavDrawerItem
            {
                Title = "Kifizetések",
                IconSource = "payments.png",
                TargetPage = newsInfoPage
            });
            masterPageItems.Add(new NavDrawerItem
            {
                Title = "Hozamok",
                IconSource = "purchases.png",
                TargetPage = newsInfoPage
            });
            masterPageItems.Add(new NavDrawerItem
            {
                Title = "Árfolyamok",
                IconSource = "exchange.png",
                TargetPage = newsInfoPage
            });
            masterPageItems.Add(new NavDrawerItem
            {
                Title = "Elérhetőségek",
                IconSource = "contact.png",
                TargetPage = newsInfoPage
            });
            masterPageItems.Add(new NavDrawerItem
            {
                Title = "Bankszámlák",
                IconSource = "bank_account.png",
                TargetPage = newsInfoPage
            });
            masterPageItems.Add(new NavDrawerItem
            {
                Title = "Névjegy",
                IconSource = "about.png",
                TargetPage = newsInfoPage
            });
            masterPageItems.Add(new NavDrawerItem
            {
                Title = "Kilépés",
                IconSource = "logout.png",
                TargetPage = newsInfoPage
            });


            ListView.ItemsSource = masterPageItems;
        }
    }
}
